import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';

import { LancamentosProvider } from '../../providers/lancamentos/lancamentos';
import { DatabaseProvider } from '../../providers/database/database';
import { ModalLancamentoPage } from '../modal-lancamento/modal-lancamento';

@IonicPage()
@Component({
  selector: 'page-lancamentos',
  templateUrl: 'lancamentos.html',
})
export class LancamentosPage {

  listaLancamentos: any = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public lancamentosProvider: LancamentosProvider, public databaseProvider: DatabaseProvider, public alertCtrl: AlertController) {
    this.databaseProvider.createDatabase(() => {
      this.lancamentosProvider.getRows((data) => {
        this.listaLancamentos = data;
      });
    });
  }

  inserirLancamento() {
    let modal = this.modalCtrl.create(ModalLancamentoPage);
    modal.onDidDismiss((data) => {
      this.lancamentosProvider.addLancamento(data, (lancamento) => {
        this.listaLancamentos.push(lancamento);
      })
    })
    modal.present();
  }

  deletar(lancamento) {
    this.confirmarExclusao(lancamento, (lancamento => {
      this.lancamentosProvider.deletar(lancamento, (lancamento) => {
        this.lancamentosProvider.getRows((lista => {
          this.listaLancamentos = lista;
        }))
      })
    }))
  }

  confirmarExclusao(lancamento, callback) {
    let alert = this.alertCtrl.create({
      title: 'Confirmação exclusão',
      message: 'Deseja realmente excluir o lancamento ' + lancamento.descricao + '?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            callback(lancamento);
          }
        }
      ]
    });
    alert.present();
  }

}
