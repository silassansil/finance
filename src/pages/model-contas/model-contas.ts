import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-model-contas',
  templateUrl: 'model-contas.html',
})
export class ModelContasPage {

  conta : any;

  constructor(public viewCtrl: ViewController, public navCtrl: NavParams) {
    this.conta = navCtrl.get("parametro") || {descricao : ""};

  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  salvar(){
    this.viewCtrl.dismiss(this.conta);
  }
}
