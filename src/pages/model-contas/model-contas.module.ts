import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModelContasPage } from './model-contas';

@NgModule({
  declarations: [
    ModelContasPage,
  ],
  imports: [
    IonicPageModule.forChild(ModelContasPage),
  ],
})
export class ModelContasPageModule {}
