import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LancamentosPage } from '../lancamentos/lancamentos';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public tab1: any = LancamentosPage

  constructor(public navCtrl: NavController) {
  }
}
