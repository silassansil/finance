import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { ContasProvider } from '../../providers/contas/contas';
@IonicPage()
@Component({
  selector: 'page-modal-lancamento',
  templateUrl: 'modal-lancamento.html',
})
export class ModalLancamentoPage {
  listaContas: any = [];
  lancamento: any = {
    "descricao": "",
    "valor": null,
    "data": new Date(),
    "contas": null,
    "entradaSaida": "",
    "pago": false
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public providerContas: ContasProvider) {
    this.providerContas.getRows((lista) => {
      this.listaContas = lista;
    })
  }

  fecharModal() {
    this.viewCtrl.dismiss();
  }

  salvar() {
    this.viewCtrl.dismiss(this.lancamento);
  }
}
