import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalLancamentoPage } from './modal-lancamento';

@NgModule({
  declarations: [
    ModalLancamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalLancamentoPage),
  ],
})
export class ModalLancamentoPageModule {}
