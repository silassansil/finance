import { Component } from '@angular/core';
import { IonicPage, ModalController, ToastController, AlertController } from 'ionic-angular';

import { ModelContasPage } from '../model-contas/model-contas';
import { ContasProvider } from '../../providers/contas/contas';

@IonicPage()
@Component({
  selector: 'page-contas',
  templateUrl: 'contas.html',
})
export class ContasPage {

  public listaContas: any = [];

  constructor(private modalCtrl: ModalController, private provider: ContasProvider, private toastCtrl: ToastController, private alertCtrl: AlertController) {
    provider.getRows((lista => {
      this.listaContas = lista;
    }));
  }

  inserir() {
    let modal = this.modalCtrl.create(ModelContasPage);
    modal.onDidDismiss((data) => {
      this.provider.addConta(data, (sucesso) => {
        this.listaContas.push(data);
        this.toastSucesso();
      });
    });
    modal.present();
  }

  editar(conta) {
    let modal = this.modalCtrl.create(ModelContasPage, { parametro: conta });
    modal.onDidDismiss((data) => {
      this.provider.editarConta(conta, (data) => {
        this.toastSucesso();
      });
    });
    modal.present();
  }

  deletar(conta) {
    this.confirmarExclusao(conta, (conta => {
      this.provider.deletar(conta, (conta) => {
        this.provider.getRows((lista => {
          this.listaContas = lista;
          this.toastSucesso();
        }))
      })
    }))
  }


  toastSucesso() {
    let toast = this.toastCtrl.create({
      message: 'Operação realizada com sucesso',
      duration: 3000
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  confirmarExclusao(conta, callback) {
    let alert = this.alertCtrl.create({
      title: 'Confirmação exclusão',
      message: 'Deseja realmente excluir a conta ' + conta.descricao + '?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            callback(conta);
          }
        }
      ]
    });
    alert.present();
  }
}
