import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { LancamentosProvider } from '../lancamentos/lancamentos';
import { ContasProvider } from '../contas/contas';

const DATABASE_FILE_NAME = 'data.db';

@Injectable()
export class DatabaseProvider {

  db: SQLiteObject;

  constructor(public contaProvider: ContasProvider, public lancamentoProvider: LancamentosProvider, public sqlite: SQLite) {
    console.log('Hello DatabaseProvider Provider');
  }

  createDatabase(callbackRetornoBanco) {
    this.sqlite.create({
      name: DATABASE_FILE_NAME,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        this.db = db;
        console.log("Database Criado!");
        this.contaProvider.createTableContas(db);
        this.lancamentoProvider.createTableLancamento(db);

        callbackRetornoBanco(db);

      })
      .catch(e => console.log(e));
  }
}
