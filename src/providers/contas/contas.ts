import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';

const CREATE_TABLE_CONTA = 'CREATE TABLE IF NOT EXISTS contas(id INTEGER PRIMARY KEY AUTOINCREMENT, descricao TEXT)';
const INSERT_CONTA = 'INSERT INTO contas(descricao) VALUES(?)';
const SELECT_TODAS_CONTAS = 'SELECT * FROM contas';
const DELETE_CONTA_POR_ID = 'DELETE FROM contas WHERE id = ?';
const UPDATE_CONTAS = 'UPDATE contas SET descricao = ? WHERE id = ?';

@Injectable()
export class ContasProvider {

  private db: SQLiteObject;

  constructor() {
    console.log('Construtor provider contas')
  }

  createTableContas(db: SQLiteObject) {
    this.db = db;
    this.db.transaction((tx) => {
      tx.executeSql(CREATE_TABLE_CONTA, []);
    })
    console.log('Tabela Contas Criada');
  }

  addConta(conta, sucessoCallback) {
    console.log("Iniciando Insert");
    if (conta) {
      this.db.executeSql(INSERT_CONTA, [conta.descricao])
        .then((data) => {

          conta.id = data.insertId;
          sucessoCallback(conta);
          console.log('insert executado conta criada!')
          console.log(JSON.stringify(data));
        })
        .catch(e => console.log(e));
    }
  }

  getRows(sucessoCallback) {
    let lista: any = [];
    this.db.executeSql(SELECT_TODAS_CONTAS, [])
      .then((data) => {

        for (var i = 0; i < data.rows.length; i++) {
          console.log(JSON.stringify(data.rows.item(i)));
          lista.push(data.rows.item(i));
        }
        console.log('Select todas as contas!');
        sucessoCallback(lista);
      })
      .catch(e => console.log(e));
  }

  deletar(conta, callbackDelete) {
    console.log(conta.id);

    this.db.executeSql(DELETE_CONTA_POR_ID, [conta.id])
      .then((conta) => {
        console.log("Delete sucesso...");
        callbackDelete(conta);
      })
      .catch(e => console.log(e));
  }

  editarConta(conta, sucessoCallback) {
    console.log("Iniciando Edição");
    console.log(conta.id);
    if (conta) {
      this.db.executeSql(UPDATE_CONTAS, [conta.descricao, conta.id])
        .then((data => {
          console.log("Edição Sucesso");
          sucessoCallback(conta);
        }))
        .catch(e => console.log(e));
    }
  }
}