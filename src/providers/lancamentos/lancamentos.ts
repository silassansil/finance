import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';

const CREATE_TABLE_LANCAMENTOS = 'CREATE TABLE IF NOT EXISTS lancamentos (id INTEGER PRIMARY KEY AUTOINCREMENT, descricao TEXT, valor REAL, data INTEGER, conta TEXT, entradaSaida TEXT, pago INTEGER)';
const INSERT_LANCAMENTOS = 'INSERT INTO lancamentos (descricao, valor, data, conta, entradaSaida, pago) VALUES (?, ?, ?, ?, ?, ?)';
const SELECT_TODOS_OS_LANCAMENTOS = 'SELECT * FROM lancamentos';
const DELETE_LANCAMENTO_POR_ID = 'DELETE FROM lancamentos WHERE id = ?';
const UPDATE_LANCAMENTO = 'UPDATE lancamentos SET descricao = ?, valor = ?, data = ?, conta = ?, entradaSaida = ?, pago = ? WHERE id = ?';

@Injectable()
export class LancamentosProvider {

  private db: SQLiteObject;

  constructor() {
    console.log('Construtor provider Lancamento');
  }

  createTableLancamento(db: SQLiteObject) {
    this.db = db;
    this.db.transaction((tx) => {
      tx.executeSql(CREATE_TABLE_LANCAMENTOS, {});
    })
    console.log('Tabela Lancamento Criada');
  }

  addLancamento(lancamento, sucessoCallback) {
    console.log("Iniciando Insert");
    if (lancamento) {
      this.db.executeSql(INSERT_LANCAMENTOS, [
        lancamento.descricao,
        lancamento.valor,
        lancamento.data,
        lancamento.conta,
        lancamento.entradaSaida,
        lancamento.pago
      ])
        .then((data) => {

          lancamento.id = data.insertId;
          sucessoCallback(lancamento);
          console.log('insert executado lancamento feito!')
          console.log(JSON.stringify(data));
        })
        .catch(e => console.log(e));
    }
  }

  getRows(sucessoCallback) {
    let lista: any = [];
    this.db.executeSql(SELECT_TODOS_OS_LANCAMENTOS, [])
      .then((data) => {
        for (var i = 0; i < data.rows.length; i++) {
          console.log(JSON.stringify(data.rows.item(i)));
          lista.push(data.rows.item(i));
        }
        console.log('Select todos os lancamento!');
        sucessoCallback(lista);
      })
      .catch(e => console.log(e));
  }

  deletar(lancamento, callbackDelete) {
    console.log(lancamento.id);

    this.db.executeSql(DELETE_LANCAMENTO_POR_ID, [lancamento.id])
      .then((lancamento) => {
        console.log("Delete sucesso...");
        callbackDelete(lancamento);
      })
      .catch(e => console.log(e));
  }

  editarLancamento(lancamento, sucessoCallback) {
    console.log("Iniciando Edição");
    console.log(lancamento.id);
    if (lancamento) {
      this.db.executeSql(UPDATE_LANCAMENTO, [
        lancamento.descricao,
        lancamento.valor,
        lancamento.data,
        lancamento.conta,
        lancamento.entradaSaida,
        lancamento.pago,
        lancamento.id
      ])
        .then((data => {
          console.log("Edição Sucesso");
          sucessoCallback(lancamento);
        }))
        .catch(e => console.log(e));
    }
  }
}