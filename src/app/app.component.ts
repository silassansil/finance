import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { HomePage } from '../pages/home/home';
import { ContasPage } from '../pages/contas/contas';
import { DatabaseProvider } from '../providers/database/database';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;
  paginas: Array<{ title: string, component: any, icone: any }>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public databaseProvider: DatabaseProvider) {
    this.iniciarComponentes(platform, statusBar, splashScreen, databaseProvider);

    this.paginas = [
      { title: 'Home', component: HomePage, icone: 'home' },
      { title: 'Contas', component: ContasPage, icone: 'logo-usd' }
    ];
  }

  openPage(pagina) {
    this.rootPage = pagina.component;
  }

  private iniciarComponentes(
    platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, databaseProvider: DatabaseProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      // this.databaseProvider.createDatabase(() => { });
    });
  }
}