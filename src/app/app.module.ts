import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { SQLite } from '@ionic-native/sqlite';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ContasPage } from '../pages/contas/contas';
import { ContasProvider } from '../providers/contas/contas';
import { ModelContasPage } from '../pages/model-contas/model-contas';
import { LancamentosPage } from '../pages/lancamentos/lancamentos';
import { LancamentosProvider } from '../providers/lancamentos/lancamentos';
import { ModalLancamentoPage } from '../pages/modal-lancamento/modal-lancamento';
import { DatabaseProvider } from '../providers/database/database';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ContasPage,
    ModelContasPage,
    LancamentosPage,
    ModalLancamentoPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      monthNames: ['Janeiro', 'Fevereiro', 'Mar\u00e7o', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
      monthShortNames: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
      dayNames: ['Domingo', 'Segunda-feira', 'Ter\u00e7a-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira'],
      dayShortNames: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex']
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ContasPage,
    ModelContasPage,
    LancamentosPage,
    ModalLancamentoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ContasProvider,
    LancamentosProvider,
    DatabaseProvider
  ]
})
export class AppModule { }
